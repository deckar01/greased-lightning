# <sub>:zap::oil:</sub> Greased Lightning <sub>:oil::zap:</sub>

A fast, asynchronous scheduler for JavaScript.

## About

You shouldn't have to compromise performance to keep the DOM jank-free.

> **Jank** is any stuttering, juddering or just plain halting that users see when a site or app isn't keeping up with the refresh rate. 
>
> http://jankfree.org/

Processing data synchronously "pauses" the browser's rendering operations and doesn't let it resume until all the data is processed.

```js
function process(data) {
  for(item in data) doStuff(item);
}
var largeDataSet = [...];
process(largeDataSet);
```

Using `setTimeout()` or `requestAnimationFrame()` can eliminate jank, but can leave the CPU idle and significantly slow down processing.

```js
var chunkSize = 32, offset = 0;
(function processChunk() {
  process(largeDataSet.slice(offset, offset + chunkSize));
  offset += chunkSize;
  if(offset < largeDataSet.length) setTimeout(processChunk, 0);
})();
```

## Strategy

Dynamically adjust the chunk size after each chunk is processed to target 60 chunks per second.

 - Assume run time is directly proportional to chunk size
   - _T<sub>i</sub> = k * C<sub>i</sub>_
 - Let the ideal time be 60frames per second
   - _T' = (1000 milliseconds) / (60 frames)_

 1. Initialize the chunk size to a value low enough to avoid jank, but high enough to serve as an accurate benchmark
 2. Measure run time _T<sub>i</sub>_ with `performance.now()`
 3. Adjust chunk size such that _C<sub>i+1</sub> = C<sub>i</sub> / (T<sub>i</sub> / T')_
   - Notice that _C<sub>i+1</sub> = C<sub>i</sub> / (T<sub>i</sub> / T')_
   - _T<sub>i+1</sub> / k = (T<sub>i</sub> / k) / (T<sub>i</sub> / T')_
   - _T<sub>i+1</sub> / k = T' / k_
   - Implies _T<sub>i+1</sub> = T'_
 4. Schedule next chunk with `requestAnimationFrame()`

## Attributions

 - Project avatar
   - Source: [http://familyguy.shoutwiki.com/](http://familyguy.shoutwiki.com/wiki/Category:Images_-_Greased-Up_Deaf_Guy)
   - License: [Attribution-ShareAlike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/)
